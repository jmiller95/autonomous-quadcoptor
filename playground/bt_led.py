#
# Sends a command to the bluetooth module telling the arduino to turn an LED on or off
#
import bluetooth
import sys
bd_addr = "00:14:03:06:14:A9" #itade address

port = 1
sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
sock.connect((bd_addr, port))
print 'Connected'
sock.settimeout(1.0)
sock.send("n")
print 'Sent data'

rec = ''
while True:
	rec += sock.recv(1024)
	rec_end = rec.find('\n')
	if rec_end != -1:
		print(rec[:rec_end])
		break

sock.close()
