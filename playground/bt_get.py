#
# Gets data from bluetooth module and prints it to console
#
import bluetooth
import sys
bd_addr = "00:14:03:06:14:A9" #itade address

port = 1
sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
sock.connect((bd_addr, port))
print 'Connected'
sock.settimeout(1.0)

rec = ''
while True:
	rec += sock.recv(1024)
	rec_end = rec.find('\n')
	if rec_end != -1:
		print(rec[:rec_end])
		rec = ''

sock.close()
